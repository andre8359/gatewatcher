#pragma once

#include <string>

namespace gatewatcher
{
    class VideoCapture
    {
    public:
        enum class ProtocolValues
        {
            HTTP = 1,
            RTSP
        };

        VideoCapture(const ProtocolValues protocol, const std::string& string);
        std::string Perform();
        ~VideoCapture() = default;

        const std::string Url;
        const ProtocolValues Protocol;

    private:
        std::string performHttp();
        std::string performRtsp();
    };

}

// http://<user>:<password>@<ip>:<port>/cgi-bin/snapshot.cgi