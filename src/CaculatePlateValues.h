#pragma once

#include <Alpr.h>

#include <filesystem>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <utility>
#include <vector>

namespace gatewatcher
{
    struct Plate
    {
        const std::string Text;
        const float OverallConfidence;

        Plate(const std::string& text, const float confidence) : Text(text), OverallConfidence(confidence){};

        Plate(const Plate& other) : Text(other.Text), OverallConfidence(other.OverallConfidence) {}

        const Plate& operator=(const Plate& other)
        {
            auto plate(other);
            std::swap(*this, plate);
            return *this;
        }
    };

    void to_json(nlohmann::json& j, const Plate& p);

    class ConfigAlpr
    {
    public:
        const std::filesystem::path ConfigFile;
        const std::filesystem::path ResourceDir;
        const std::string Region = "br2";
        const std::string Country = "br";
        const uint32_t NumberOfCandidates = 3;
        ConfigAlpr(const std::string& configFile, const std::string& resourceDir, const std::string& region = "br2",
                   const std::string country = "br", const uint32_t numberOfCandidates = 3);

        const std::unique_ptr<alpr::Alpr>& GetAlprObj() const;

    private:
        std::unique_ptr<alpr::Alpr> alprObj;
    };

    class CaculatePlateValues
    {
    public:
        CaculatePlateValues() = delete;

        static std::vector<Plate> Calc(const ConfigAlpr& configAlpr, const std::string& imagePath);
        static std::vector<Plate> Calc(const ConfigAlpr& configAlpr, const std::vector<char>& imagePath);

    private:
        static std::string postProcess(const std::string& plate, const std::string& regex);
        static char convertNumber2Letter(const char number);
        static char convertLetter2Number(const char letter);
    };
}