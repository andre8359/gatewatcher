#include "CaculatePlateValues.h"

#include <cctype>
#include <fstream>
#include <iostream>
#include <range/v3/algorithm/transform.hpp>
#include <range/v3/iterator/insert_iterators.hpp>
#include <range/v3/to_container.hpp>
#include <range/v3/view/transform.hpp>
#include <vector>

namespace gatewatcher
{
    ConfigAlpr::ConfigAlpr(const std::string& configFile, const std::string& resourceDir, const std::string& region,
                           const std::string country, const uint32_t numberOfCandidates)
        : ConfigFile(configFile),
          ResourceDir(resourceDir),
          Region(region),
          Country(country),
          NumberOfCandidates(numberOfCandidates)
    {
        if (!std::filesystem::exists(ConfigFile))
        {
            throw std::runtime_error("File not found. ConfigFile: " + ConfigFile.string());
        }

        if (!std::filesystem::exists(ResourceDir))
        {
            throw std::runtime_error("File not found. ResourceDir: " + ResourceDir.string());
        }

        alprObj = std::make_unique<alpr::Alpr>(Country, ConfigFile.string(), ResourceDir.string());

        if (!alprObj->isLoaded())
        {
            throw std::runtime_error("Alpr object not loaded!");
        }

        alprObj->setTopN(NumberOfCandidates);
    }

    void to_json(nlohmann::json& j, const Plate& p)
    {
        j = nlohmann::json{{"Text", p.Text}, {"OverallConfidence", p.OverallConfidence}};
    }

    const std::unique_ptr<alpr::Alpr>& ConfigAlpr::GetAlprObj() const { return alprObj; }

    std::vector<Plate> CaculatePlateValues::Calc(const ConfigAlpr& configAlpr, const std::string& imagePath)
    {
        const auto& openalpr = configAlpr.GetAlprObj();

        alpr::AlprResults results = openalpr->recognize(imagePath);

        if (results.plates.empty())
        {
            std::cout << "No match" << std::endl;
            return {};
        }

        if (results.plates.size() > 1)
        {
            std::cout << "To much match" << std::endl;
            return {};
        }

        const std::string regex = (configAlpr.Region == "br") ? "@@@$$$$" : "@@@$@$$";

        const auto& plate = results.plates.front();

        std::cout << "plate"
                  << ": " << plate.topNPlates.size() << " results" << std::endl;

        return plate.topNPlates | ranges::views::transform([&regex](const auto& candidate) {
                   return Plate(postProcess(candidate.characters, regex), candidate.overall_confidence);
               }) |
               ranges::to<std::vector>;
    }

    std::vector<Plate> CaculatePlateValues::Calc(const ConfigAlpr& configAlpr, const std::vector<char>& imagePath)
    {
        const auto& openalpr = configAlpr.GetAlprObj();

        alpr::AlprResults results = openalpr->recognize(imagePath);

        if (results.plates.empty())
        {
            std::cout << "No match" << std::endl;
            return {};
        }

        if (results.plates.size() > 1)
        {
            std::cout << "To much match" << std::endl;
            return {};
        }

        const auto& plate = results.plates.front();

        const std::string regex = (configAlpr.Region == "br") ? "@@@$$$$" : "@@@$@$$";
        return plate.topNPlates | ranges::views::transform([&regex](const auto& candidate) {
                   return Plate(postProcess(candidate.characters, regex), candidate.overall_confidence);
               }) |
               ranges::to<std::vector>();
    }

    std::string CaculatePlateValues::postProcess(const std::string& plate, const std::string& regex)
    {
        if (std::size(plate) != std::size(regex))
        {
            return plate;
        }

        std::string result;
        ranges::transform(plate, regex, ranges::back_inserter(result), [](const auto& ch, const auto& reg) -> char {
            if ((reg == '@') && !std::isalpha(ch))
            {
                return convertNumber2Letter(ch);
            }

            if ((reg == '$') && !std::isdigit(ch))
            {
                return convertLetter2Number(ch);
            }

            return ch;
        });

        return result;
    }

    char CaculatePlateValues::convertNumber2Letter(const char number)
    {
        switch (number)
        {
            case '1':
                return 'I';
            case '0':
                return 'O';
            default:
                return number;
        }
    }

    char CaculatePlateValues::convertLetter2Number(const char letter)
    {
        switch (letter)
        {
            case 'O':
                return '0';
            case 'I':
                return '1';
            default:
                return letter;
        }
    }
}