#include "VideoCapture.h"

#include <cpr/cpr.h>

#include <iostream>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio/videoio.hpp>


namespace gatewatcher
{
    VideoCapture::VideoCapture(const VideoCapture::ProtocolValues protocol, const std::string& str)
        : Protocol(protocol), Url(str)
    {
    }

    std::string VideoCapture::Perform()
    {
        switch (Protocol)
        {
            case VideoCapture::ProtocolValues::HTTP:
            {
                return performHttp();
            }

            case VideoCapture::ProtocolValues::RTSP:
            {
                return performRtsp();
            }
        }

        return "";
    }

    std::string VideoCapture::performHttp()
    {
        std::cout << "Requesting camera image in " << Url << std::endl;
        const auto r = cpr::Get(cpr::Url{Url});
        return r.text;
    }

    std::string VideoCapture::performRtsp()
    {
        std::cout << "Requesting camera image in " << Url << std::endl;

        cv::VideoCapture videoCapture(Url);

        if (!videoCapture.isOpened())
        {
            std::cout << "Error opening video stream or file";
            return "";
        }

        cv::Mat image;

        if (!videoCapture.read(image))
        {
            std::cout << "Error opening video stream or file";

            return "";
        }

        std::vector<uint8_t> buff;
        cv::imencode(".jpg", image, buff);

        return std::string(std::cbegin(buff), std::cend(buff));
    }
}