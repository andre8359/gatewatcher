#include "GateWatcher.h"

#include <ctime>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <restinio/all.hpp>

#include "VideoCapture.h"

namespace gatewatcher
{
    GateWatcher::GateWatcher()
        : m_brConfigAlpr("../resources/openalpr.conf", "../resources/runtime_data", "br", "br", 1),
          m_br2ConfigAlpr("../resources/openalpr.conf", "../resources/runtime_data", "br2", "br", 1)
    {
        LoadConfigFile();
    }

    void GateWatcher::Run()
    {
        std::cout << "Running ...." << std::endl;
        restinio::run(
            restinio::on_this_thread()
                .port(m_configFile.at("ServerPort").get<uint16_t>())
                .address(m_configFile.at("ServerAddress").get<std::string>())
                .request_handler([this](auto req) { return req->create_response().set_body(this->run()).done(); }));
    }

    void GateWatcher::LoadConfigFile()
    {
        std::ifstream configFile("../config/gatewatcher.json");

        if (!configFile.is_open())
        {
            throw std::runtime_error("Couldn't read configFile!");
        }

        std::string fileContent;

        configFile.seekg(0, std::ios::end);
        fileContent.reserve(configFile.tellg());
        configFile.seekg(0, std::ios::beg);

        fileContent.assign((std::istreambuf_iterator<char>(configFile)), std::istreambuf_iterator<char>());

        m_configFile = nlohmann::json::parse(fileContent);

        if (m_configFile.at("ServerAddress").get<std::string>().empty())
        {
            throw std::runtime_error("No ServerAddress info!");
        }

        if (!m_configFile.at("ServerPort").get<uint32_t>())
        {
            throw std::runtime_error("No ServerPort info!");
        }

        if (m_configFile.at("CameraUrl").get<std::string>().empty())
        {
            throw std::runtime_error(
                "No CameraUrl info!Format: http://<user>:<password>@<ip>:<port>/cgi-bin/snapshot.cgi");
        }

        if (!m_configFile.at("CameraProtocol").get<uint8_t>())
        {
            throw std::runtime_error("No CameraProtocol: Http:1 / RTSP:2");
        }

        const auto pictureFolderPath = m_configFile.at("PicturesFolder").get<std::string>();

        if (pictureFolderPath.empty())
        {
            throw std::runtime_error("No PicturesFolder info!");
        }

        if (!std::filesystem::exists(pictureFolderPath))
        {
            throw std::runtime_error("PicturesFolder not exist. PicturesFolder: " + pictureFolderPath);
        }
    }

    std::string GateWatcher::run()
    {
        std::cout << "Request received!" << std::endl;

        VideoCapture videoCapture(static_cast<VideoCapture::ProtocolValues>(m_configFile.at("CameraProtocol")),
                                  m_configFile.at("CameraUrl").get<std::string>());

        const auto image = videoCapture.Perform();

        if (image.empty())
        {
            std::cout << "Couldn't get camera image!" << std::endl;
            return "";
        }

        std::cout << "Calculating image plate;.." << std::endl;
        const std::vector<char> bytesImage(std::cbegin(image), std::cend(image));
        const auto brRes = CaculatePlateValues::Calc(m_brConfigAlpr, bytesImage);
        const auto br2Res = CaculatePlateValues::Calc(m_br2ConfigAlpr, bytesImage);

        std::map<std::string, std::vector<Plate>> response;
        response["br"] = brRes;
        response["br2"] = br2Res;

        const auto datetime = currentDatetime();
        const auto brPlateText = brRes.empty() ? "0" : brRes.front().Text;
        const auto br2PlateText = br2Res.empty() ? "0" : br2Res.front().Text;

        const auto filename = m_configFile.at("PicturesFolder").get<std::string>() + "/" + datetime + "_" +
                              brPlateText + "_" + br2PlateText + ".jpg";

        nlohmann::json responseJson = response;

        std::cout << "Filename: " << filename << std::endl;

        std::ofstream fileImage(filename, std::ios::binary | std::ios::ate);

        if (!fileImage.is_open())
        {
            throw std::runtime_error("Couldn't open fileImage on: " + filename);
        }

        fileImage << image;

        std::cout << responseJson.dump(4) << std::endl;
        return responseJson.dump(4);
    }

    std::string GateWatcher::currentDatetime()
    {
        time_t rawtime;
        struct tm* timeinfo;
        char buffer[80];
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        strftime(buffer, sizeof(buffer), "%Y%m%d_%H%M%S", timeinfo);
        return buffer;
    }
}