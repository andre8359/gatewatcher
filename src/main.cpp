#include <fstream>
#include <iostream>

#include "CaculatePlateValues.h"
#include "GateWatcher.h"
#include "VideoCapture.h"

int main()
{
    try
    {
        gatewatcher::GateWatcher obj;
        obj.Run();
    }
    catch (const std::exception& e)
    {
        std::cout << e.what();
    }

    return 0;
}
